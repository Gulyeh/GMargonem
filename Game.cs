﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace GMargonem
{
    class Game
    {
        public static string Token = string.Empty;
        public static string Ev = string.Empty;
        public static List<arrowitem> strzaly = new List<arrowitem>();
        public static List<healitem> heals = new List<healitem>();
        public static List<expbless> expbon = new List<expbless>();
        public static bool ExpBless { get; set; }

        //GET VARIABLLE
        public static string prof { get; set; }
        public static int lvl { get; set; }

        //ITEMS VARIABLES
        public static int iloscpotek = 0;
        public static int iloscstrzal = 0;
        public static bool usedbless = false;
        public static int BlessTimeLeft = 0;


        public static async Task<string> GetGameDetails(string server, string charid, string initlevel)
        {
            Start:
                try
                {
                    int level = int.Parse(initlevel);
                    string type = string.Empty;
                    using (HttpClientHandler handler = new HttpClientHandler
                    {
                        CookieContainer = Cookies.Cookie
                    })
                    {
                    using (HttpClient client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Dalvik/1.6.0 (Linux; U; Android 4.4.2; GT-I9500 Build/KOT49H)");

                        switch (level)
                        {
                            case 1:
                                {
                                  type = "http://" + server + ".margonem.pl/engine?t=init&initlvl=" + initlevel + "&mucka=" + Mucka.GenerateMucka() + "&mobile=1";
                                  break;
                                }
                            default:
                                {
                                  type = "http://" + server + ".margonem.pl/engine?t=init&initlvl=" + initlevel + "&mucka=" + Mucka.GenerateMucka() + "&mobile=1&mobile_token=" + Encryptor.Md5(Token, "humantorch-");
                                  break;
                                }
                        }
                        
                        Uri url = new Uri(type);
                        Cookies.Cookie.Add(url, new Cookie("mchar_id", charid));
                        string response = await client.GetStringAsync(url);

                        var json = JObject.Parse(response);

                        switch (level)
                        {
                            case 1:
                                {
                                    if(response.Contains("mobile_token"))
                                    {
                                        Token = json["mobile_token"].ToString();
                                    }
                                    break;
                                }
                            case 3:
                                {
                                    await GetItems(response);
                                    break;
                                }
                            case 4:
                                {
                                    if (response.Contains("ev"))
                                    {
                                        Ev = json["ev"].ToString();
                                    }
                                    break;
                                }
                        }
                        return response;
                    }
                    }
                }
                catch (Exception)
                {
                    await Task.Delay(4000);
                    goto Start;
                }
        }

        private static Task<bool> GetItems(string items)
        {
            Start:
            try
            {
                iloscpotek = 0;
                iloscstrzal = 0;
                BlessTimeLeft = 0;
                usedbless = false;
                strzaly.Clear();
                heals.Clear();
                expbon.Clear();

                var json = JsonConvert.DeserializeObject<Inventory>(items);

                foreach (KeyValuePair<string, Itemz> data in json.Item)
                {
                    //GET ARROWS INFO
                    if (prof == "t" || prof == "h")
                    {
                        var regex1 = Regex.Match(data.Value.stat, "ammo=(.*?);");
                        var regex2 = Regex.Match(data.Value.stat, "lvl=(.*?);");

                        if (regex1.Success && data.Value.st == 0)
                        {
                            if (regex2.Success)
                            {
                                int ammolevel = Int32.Parse(regex2.Groups[1].Value);

                                if (ammolevel <= lvl)
                                {
                                    arrowitem AddArrow = new arrowitem
                                    {
                                        id = Int32.Parse(data.Key),
                                        val = Int32.Parse(regex1.Groups[1].Value)
                                    };
                                    strzaly.Add(AddArrow);

                                }
                            }
                            else
                            {
                                arrowitem AddArrow = new arrowitem
                                {
                                    id = Int32.Parse(data.Key),
                                    val = Int32.Parse(regex1.Groups[1].Value)
                                };
                            }

                            iloscstrzal += Int32.Parse(regex1.Groups[1].Value);
                        }
                    }

                    //GET POTS INFO
                    var regex3 = Regex.Match(data.Value.stat, "leczy=(.*?);");
                    var regex5 = Regex.Match(data.Value.stat, "amount=(.*?);");
                    var regex6 = Regex.Match(data.Value.stat, "leczy=(.*)");

                        if (regex3.Success && regex5.Success)
                        {
                            healitem HealItem = new healitem
                            {
                                amount = Int32.Parse(regex5.Groups[1].Value),
                                heal = Int32.Parse(regex3.Groups[1].Value),
                                id = Int32.Parse(data.Key)
                            };
                            heals.Add(HealItem);
                            iloscpotek += Int32.Parse(regex5.Groups[1].Value);
                        }
                        else if (regex6.Success && regex5.Success)
                        {
                            healitem HealItem = new healitem
                            {
                                amount = Int32.Parse(regex5.Groups[1].Value),
                                heal = Int32.Parse(regex3.Groups[1].Value),
                                id = Int32.Parse(data.Key)
                            };
                            heals.Add(HealItem);
                            iloscpotek += Int32.Parse(regex5.Groups[1].Value);
                        }

                    //GET EXP BON INFO
                    var regexexp = Regex.Match(data.Value.stat, "npc_expbon=(.*?);");
                    var time = Regex.Match(data.Value.stat, "ttl=(.*)");
                    var amountbon = Regex.Match(data.Value.stat, "amount=(.*?);");

                    if (regexexp.Success && time.Success && amountbon.Success && data.Value.st == 0 && ExpBless == true)
                    {
                        expbless bless = new expbless
                        {
                            exppercent = Int32.Parse(regexexp.Groups[1].Value),
                            amount = Int32.Parse(amountbon.Groups[1].Value),
                            czas = Int32.Parse(time.Groups[1].Value),
                            id = Int32.Parse(data.Key)
                        };
                        expbon.Add(bless);
                    }
                    else if (regexexp.Success && time.Success && amountbon.Success && data.Value.st != 0 && ExpBless == true)
                    {
                        usedbless = true;
                        BlessTimeLeft = Int32.Parse(time.Groups[1].Value);
                    }
                }

                return Task.FromResult(true);

            }catch(Exception)
            {
                goto Start;
            }
        }

        //ITEMS VALUES
        internal class healitem
        {
            public int amount { get; set; }
            public int heal { get; set; }
            public int id { get; set; }
        }

        internal class arrowitem
        {
            public int id { get; set; }
            public int val { get; set; }
        }

        internal class expbless
        {
            public int exppercent { get; set; }
            public int amount { get; set; }
            public int czas { get; set; }
            public int id { get; set; }
        }

        public class Inventory
        {
            [JsonProperty(PropertyName = "item")]
            public Dictionary<string, Itemz> Item { get; set; }
        }

        public class Itemz
        {
            [JsonProperty(PropertyName = "cl")]
            public int cl { get; set; }

            [JsonProperty(PropertyName = "icon")]
            public string icon { get; set; }

            [JsonProperty(PropertyName = "loc")]
            public string loc { get; set; }

            [JsonProperty(PropertyName = "name")]
            public string name { get; set; }

            [JsonProperty(PropertyName = "own")]
            public int own { get; set; }

            [JsonProperty(PropertyName = "st")]
            public int st { get; set; }

            [JsonProperty(PropertyName = "stat")]
            public string stat { get; set; }

            [JsonProperty(PropertyName = "x")]
            public int x { get; set; }

            [JsonProperty(PropertyName = "y")]
            public int y { get; set; }
        }

    }
}
