﻿using System;
using System.Text;
using System.Xml.Linq;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Linq;
using System.Xml;

namespace GMargonem
{
    class SaveSettings
    {
        public static List<string>[] id { get; set; }
        public static List<SettVar> LoadXML = new List<SettVar>();
        public class SettVar
        {
            public string id;
            public bool autoheal;
            public bool checkpots;
            public string hpval;
            public bool sellitems;
            public bool sellpots;
            public bool equiparrow;
            public bool checkarrow;
            public bool usestamina;
            public bool usebon;
            public int mapindex;
        }

        public static void SaveData(int charcount)
        {
            XmlTextWriter writer = new XmlTextWriter("Settings-"+BotWindow.login+".xml", Encoding.UTF8);

            writer.WriteStartDocument();
            writer.WriteWhitespace(Environment.NewLine);

            //start settings
            writer.WriteStartElement("settings");
            writer.WriteWhitespace(Environment.NewLine);

            for (int i = 0; i < charcount; i++)
            {
                var json = JObject.Parse(id[i][0].ToString());

                //start char
                writer.WriteString("\t");
                writer.WriteStartElement("character");
                writer.WriteWhitespace(Environment.NewLine);

                //start id
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("id");
                writer.WriteString(json["h"]["id"].ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end id

                //start map
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("map");
                writer.WriteString(BotWindow.setting[i].mapindex.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end map

                //start heal
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("heal");
                writer.WriteString(BotWindow.setting[i].autoheal.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end heal

                //start checkpots
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("checkpotions");
                writer.WriteString(BotWindow.setting[i].checkpots.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end checkpots

                //start hp
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("hp");
                writer.WriteString(BotWindow.setting[i].hpval);
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end hp

                //start sellitems
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("sellitems");
                writer.WriteString(BotWindow.setting[i].sellitems.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end sellitems

                //start sellpots
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("sellpots");
                writer.WriteString(BotWindow.setting[i].sellpots.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end sellpots

                //start eqarrows
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("equiparrows");
                writer.WriteString(BotWindow.setting[i].equiparrow.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end eqarrows

                //start checkarrows
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("checkarrows");
                writer.WriteString(BotWindow.setting[i].checkarrow.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end checkarrows

                //start usestamina
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("usestamina");
                writer.WriteString(BotWindow.setting[i].usestamina.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end usestamina

                //start usebless
                writer.WriteString("\t");
                writer.WriteString("\t");
                writer.WriteStartElement("usebless");
                writer.WriteString(BotWindow.setting[i].usebon.ToString());
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
                //end usebless

                //end char
                writer.WriteString("\t");
                writer.WriteEndElement();
                writer.WriteWhitespace(Environment.NewLine);
            }

            //end settings
            writer.WriteEndElement();
            writer.WriteWhitespace(Environment.NewLine);

            //end document
            writer.WriteEndDocument();
            writer.Close();
        }

        public static Task<bool> ReadSettings()
        {
            if (File.Exists("Settings-" + BotWindow.login + ".xml"))
            {
                XDocument xml = XDocument.Load("Settings-" + BotWindow.login + ".xml");
                var nodes = (from n in xml.Descendants("character")
                                 select new
                                 {
                                     id = n.Element("id").Value,
                                     map = n.Element("map").Value,
                                     heal = n.Element("heal").Value,
                                     checkpotions = n.Element("checkpotions").Value,
                                     hp = n.Element("hp").Value,
                                     sellitems = n.Element("sellitems").Value,
                                     sellpots = n.Element("sellpots").Value,
                                     equiparrows = n.Element("equiparrows").Value,
                                     checkarrows = n.Element("checkarrows").Value,
                                     usestamina = n.Element("usestamina").Value,
                                     usebless = n.Element("usebless").Value,
                                 }).ToList();

                foreach (var n in nodes)
                {
                    SettVar item = new SettVar
                    {
                        id = n.id,
                        autoheal = Convert.ToBoolean(n.heal),
                        checkpots = Convert.ToBoolean(n.checkpotions),
                        hpval = n.hp,
                        sellitems = Convert.ToBoolean(n.sellitems),
                        sellpots = Convert.ToBoolean(n.sellpots),
                        equiparrow = Convert.ToBoolean(n.equiparrows),
                        checkarrow = Convert.ToBoolean(n.checkarrows),
                        usestamina = Convert.ToBoolean(n.usestamina),
                        usebon = Convert.ToBoolean(n.usebless),
                        mapindex = int.Parse(n.map)
                    };
                    LoadXML.Add(item);
                }

            }
            return Task.FromResult(true);
        }
    }
}
