﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace GMargonem
{
    class Stats
    {
        public async static Task<string> GetCharInfo(string charid)
        {
        Start:
            try
            {
                using (HttpClientHandler handler = new HttpClientHandler
                {
                    CookieContainer = Cookies.Cookie
                })
                {
                    using (HttpClient client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Dalvik/1.6.0 (Linux; U; Android 4.4.2; GT-I9500 Build/KOT49H)");
                        Uri url = new Uri("http://www.margonem.pl/ajax/getplayerdata.php?app_version=1.0.13");
                        Cookies.Cookie.Add(url, new Cookie("mchar_id", charid));
                        string Result = await client.GetStringAsync(url);
                        return Result;
                    }                  
                }
            }catch(Exception)
            {
                goto Start;
            }
        }
    }
}
