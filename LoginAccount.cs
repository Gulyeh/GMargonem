﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;

namespace GMargonem
{
    internal class LoginAccount
    {
        public static async Task<string> LogintoAccount(string username, string password)
        {
            Start:
                try
                {
                    string Info = string.Empty;
                    using (HttpClientHandler handler = new HttpClientHandler
                    {
                        CookieContainer = Cookies.Cookie
                    })
                    {
                        using (HttpClient client = new HttpClient(handler))
                        {
                            client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Dalvik/1.6.0 (Linux; U; Android 4.4.2; GT-I9500 Build/KOT49H)");

                            var Content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("l", username),
                            new KeyValuePair<string, string>("ph", Encryptor.ToSha("mleczko"+password))
                        });

                            HttpResponseMessage response = await client.PostAsync("http://www.margonem.pl/ajax/logon.php?t=login", Content);
                            Info = await response.Content.ReadAsStringAsync();
                        }
                    }
                    return Info;
                }catch(Exception)
                {
                    goto Start;
                }
        }
    }
}
