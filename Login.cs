﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.IO;

namespace GMargonem
{
    public partial class LoginTop : Form
    {

        //VARIABLES, ETC.
        Loading loading;
        loginmsg msg;
        AddCharMessage charmsg;
        BotWindow exp;

        public List<Item> CharList = new List<Item>();
        public List<CharDetails> CharInfo = new List<CharDetails>();
        public int data = 0;
        public List<string>[] mapnames = new List<string>[1000];
        string[] mapid = new string[] { "2675", "2676", "2677", "2794", "2795", "2796", "2797", "2798", "2799", "2800", "2801", "2802", "2803", "2804", "2805", "2806", "2807", "2808", "2809", "2810", "2829", "2830", "2831", "2832", "2833", "2834", "2835", "2836", "2837", "2838", "2839", "2840", "2841", "2842", "2843", "2844", "2845", "2846", "2847", "2848", "2849", "2850", "2851", "2852", "2853", "2854", "2855", "2856", "2857", "2858", "2859" };
        public List<string>[] detailedinfo = new List<string>[10000];


        //MAIN FUNCTION
        public LoginTop()
        {
            InitializeComponent();
            charpanel.Width = 10;
        }


        //LOAD APP
        private void Login_Load(object sender, EventArgs e)
        {
            loadcredits();
            this.Height = 34;
            timer3.Start();
        }

        //BUTTONS CLICKS
        public void exitapp_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void minimalize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void passtext_Enter(object sender, EventArgs e)
        {
            if (passtext.Text == String.Empty)
            {
                passtext.Text = "";
                passtext.isPassword = true;
            }
        }

        private async void loginbutton_Click(object sender, EventArgs e)
        {
            string pass = passtext.Text;
            string login = logintext.Text;

            showloading();

            await Task.Delay(2000);

            string Login = await LoginAccount.LogintoAccount(login, pass);

            showaction();

            if (Login.Contains("logged"))
            {
                if (savedetails.Checked == true)
                {
                    Details.SaveData(login, pass);
                }

                BotWindow.login = login;
                actiontext(true, false, false, false);
                msg.Show(this);
                await Charlist(Login);
                await Task.Delay(2000);
                timer5.Start();
            }
            else if (Login.Contains("zablokowane"))
            {
                actiontext(false, true, false, false);
                msg.Show(this);
                await Task.Delay(2000);
                timer4.Start();
            }
            else if (Login.Contains("Przerwa"))
            {
                actiontext(false, false, false, true);
                msg.Show(this);
                await Task.Delay(2000);
                timer4.Start();
            }
            else
            {
                actiontext(false, false, true, false);
                msg.Show(this);
                await Task.Delay(2000);
                timer4.Start();
            }

            this.TopMost = true;
            msg.Dispose();
            this.TopMost = false;
        }

        private void passtext_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                loginbutton.PerformClick();
            }
        }

        private void logintext_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                loginbutton.PerformClick();
            }
        }


        //TIMERS
        private void timer1_Tick(object sender, EventArgs e)
        {
            if(this.Opacity > 0.5)
            {
                this.Opacity -= 0.025; 
            }
            else if(this.Opacity <= 0.5)
            {
                timer1.Stop();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if(loading.Opacity < 1)
            {
                loading.Opacity += 0.05;
            }
            else
            {
                timer2.Stop();
            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (this.Height < 268)
            {
                this.Height += 24;
            }
            else
            {
                timer3.Stop();
            }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            if (msg.Opacity > 0.025)
            {
                msg.Opacity -= 0.025;
                this.Opacity += 0.05;
            }
            else if (msg.Opacity <= 0.03)
            {
                this.TopMost = true;
                msg.Hide();
                this.Enabled = true;
                this.Opacity = 1;
                this.TopMost = false;
                timer4.Stop();
            }

        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            if (msg.Opacity > 0.05)
            {
                msg.Opacity -= 0.05;
                this.Opacity += 0.025;
            }
            else if (msg.Opacity <= 0.05)
            {
                this.TopMost = true;
                msg.Hide();
                this.TopMost = false;
                this.Opacity = 1;
                charpanel.Width = 297;
                this.Enabled = true;
                timer6.Start();
                timer5.Stop();
            }
        }
   
        private void timer6_Tick(object sender, EventArgs e)
        {
            if (this.Height > 216)
            {
                this.Height -= 13;
            }
            else
            {
                timer6.Stop();
            }
        }

        private void timer7_Tick(object sender, EventArgs e)
        {
            if(charmsg.Opacity < 1)
            {
                charmsg.Opacity += 0.05;
                this.Opacity -= 0.025;
            }
            else if(charmsg.Opacity == 1)
            {
                timer7.Stop();
            }
        }

        private void timer8_Tick(object sender, EventArgs e)
        {
            if(charmsg.Opacity > 0.05)
            {
                charmsg.Opacity -= 0.05;
                this.Opacity += 0.025;
            }
            else if(charmsg.Opacity <= 0.05)
            {
                charmsg.Opacity = 0;
                this.TopMost = true;
                charmsg.Close();
                this.TopMost = false;
                this.Opacity = 1;
                this.Enabled = true;
                timer8.Stop();
            }
        }

        private async void timer9_Tick(object sender, EventArgs e)
        {
            if (this.Opacity > 0.05)
            {
                this.Opacity -= 0.05;
            }
            else if (this.Opacity < 0.05)
            {
                this.Opacity = 0;
                exp = new BotWindow();
                exp.StartPosition = FormStartPosition.CenterScreen;
                await passdata();
                exp.Show();
                this.Hide();
                timer9.Stop();
            }
        }


        //MESSAGE BOXES
        private void showloading()
        {
            loading = new Loading();
            timer1.Start();
            loading.ShowInTaskbar = false;
            loading.StartPosition = FormStartPosition.Manual;
            loading.Location = new Point(this.Location.X + (this.Width - loading.Width) / 2, this.Location.Y + (this.Height - loading.Height) / 2);
            loading.Opacity = 0;
            this.Enabled = false;
            timer2.Start();
            loading.Show(this);
        }

        private void showaction()
        {
            msg = new loginmsg();
            this.TopMost = true;
            loading.Dispose();
            this.TopMost = false;
            msg.Opacity = 1;
            msg.ShowInTaskbar = false;
            msg.StartPosition = FormStartPosition.Manual;
            msg.Location = new Point(this.Location.X + (this.Width - loading.Width) / 2, (this.Location.Y + 50) + (this.Height - loading.Height) / 2);
        }

        private void actiontext(bool one, bool two, bool three, bool four)
        {
            msg.loginin.Visible = one;
            msg.blocktext.Visible = two;
            msg.wrongpass.Visible = three;
            msg.techbreak.Visible = four;
        }


        //LOAD CREDITENTIALS
        private void loadcredits()
        {
            string credits = Details.readxml();
            logintext.Text = Regex.Split(credits, "-")[0].ToString();
            passtext.Text = Regex.Split(credits, "-")[1].ToString();
            passtext.isPassword = true;
        }


        //GET CHARACTERS INFO
        public class Item
        {
            public string ID;
            public string Name;
        }

        private Task<bool> Charlist(string Details)
        {

            string pattern = "option label=\"(.*?)\" value=\"(.*?)\"";
            Regex regex = new Regex(pattern, RegexOptions.None);
            foreach (Match match in regex.Matches(Details))
            {
                if (match.Success)
                {
                    Item item = new Item
                    {
                        Name = match.Groups[1].Value,
                        ID = match.Groups[2].Value
                    };

                    CharList.Add(item);
                }
            }

            CharList.ForEach(item => listofchars.Items.Add(item.Name));

            if (CharList.Count > 0)
            {
                listofchars.SelectedIndex = 0;
            }

            return Task.FromResult(true);
        }


        //ADD CHARACTER
        public class CharDetails
        {
            public string id;
            public string nick;
            public string level;
            public string server;
            public string prof;
        }

        private void charmsgtext(bool one, bool two, bool three, bool four, bool five, bool six)
        {
            charmsg.adding.Visible = one;
            charmsg.added.Visible = two;
            charmsg.techbreak.Visible = three;
            charmsg.alreadyadded.Visible = four;
            charmsg.error.Visible = five;
            charmsg.tryagain.Visible = six;

            if (one == true)
            {
                charmsg.panel1.Visible = true;
            }
            else
            {
                charmsg.panel1.Visible = false;
            }
        }

        private void showcharmsg()
        {
            this.Enabled = false;
            charmsg = new AddCharMessage();
            charmsg.ShowInTaskbar = false;
            charmsg.StartPosition = FormStartPosition.Manual;
            charmsg.Location = new Point(this.Location.X + (this.Width - loading.Width) / 2, (this.Location.Y + 50) + (this.Height - loading.Height) / 2);
            charmsg.Opacity = 0;
            timer7.Start();
            charmsg.Show(this);
        }

        private Task<bool> CheckAvailableMaps(string maps)
        {
            Start:
                try
                {
                    mapnames[data] = new List<string>();

                    var json = JObject.Parse(maps);
                    int available = 1;

                    for (int i = 0; i < mapid.Length; i++)
                    {
                        if(available == 1)
                        {
                            mapnames[data].Add(json["mobile_maps"][mapid[i]]["name"].ToString());
                            available = json["mobile_maps"][mapid[i]]["done"].Value<int>();
                        }
                    }
                
                    return Task.FromResult(true);

                }catch(Exception)
                {
                    goto Start;
                }
        }

        private async void addchar_Click(object sender, EventArgs e)
        {
            string pass = passtext.Text;
            string login = logintext.Text;
            showcharmsg();
            await Task.Delay(1100);
            int retry = 0;

            Start:
            try
            {
                retry++;
                if (retry < 5)
                {
                    //VARIABLES
                    bool added = false;
                    string charid = CharList[listofchars.SelectedIndex].ID;

                    //GET DETAILS
                    string details = await Stats.GetCharInfo(charid);

                    //FUNCTIONS
                    var json = JObject.Parse(details);
                    if (json["ok"].Value<bool>() == true && json["charlist"].HasValues)
                    {
                        //CHECK IF CHARACTER IS ALREADY ADDED
                        for (int i = 0; i < data; i++)
                        {
                            if (CharInfo[i].id == charid)
                            {
                                added = true;
                                break;
                            }
                        }

                        //START ADDING
                        if (added == false)
                        {

                            //GET MORE INFO
                            CharDetails info = new CharDetails
                            {
                                id = charid,
                                nick = json["charlist"][charid]["nick"].ToString(),
                                level = json["charlist"][charid]["lvl"].ToString(),
                                prof = json["charlist"][charid]["prof"].ToString(),
                                server = json["charlist"][charid]["db"].ToString().Trim(new Char[] { '#' })
                            };

                            CharInfo.Add(info);

                            //RELOG TO NEXT CHARACTER IF NEEDED
                            if (data > 0)
                            {
                                await Game.GetGameDetails(CharInfo[data].server, CharInfo[data].id, "1");
                                await Task.Delay(8000);
                            }

                            //GET MAPS
                            string flag2 = await Game.GetGameDetails(CharInfo[data].server, CharInfo[data].id, "1");
                            await Task.Delay(1100);
                            string maps = await Game.GetGameDetails(CharInfo[data].server, CharInfo[data].id, "2");


                            if (maps.Contains("mobile_maps"))
                            {
                                detailedinfo[data] = new List<string>();
                                detailedinfo[data].Add(flag2);
                                await CheckAvailableMaps(maps);
                                data++;
                                charmsgtext(false, true, false, false, false, false); // ADDED
                                await Task.Delay(1100);
                                timer8.Start();
                            }
                            else if (maps.Contains("Przerwa"))
                            {
                                charmsgtext(false, false, true, false, false, false); //TECH BREAK
                                await Task.Delay(1100);
                                timer8.Start();
                            }
                            else
                            {
                                //ERROR CASE
                                CharInfo.RemoveAll(r => r.id == charid);
                                await Task.Delay(1100);
                                goto Start;
                            }
                        }
                        else
                        {
                            charmsgtext(false, false, false, true, false, false); //ALREADY ADDED
                            await Task.Delay(1100);
                            timer8.Start();
                        }
                    }
                    else if (details.Contains("Przerwa"))
                    {
                        charmsgtext(false, false, true, false, false, false); //TECH BREAK
                        await Task.Delay(1100);
                        timer8.Start();
                    }
                    else
                    {
                        await Task.Delay(1100);
                        goto Start;
                    }
                }

                else
                {
                    charmsgtext(false, false, false, false, false, true); //TRY AGAIN
                    await Task.Delay(1100);
                    timer8.Start();
                }
            }
            catch (Exception)
            {
                await Task.Delay(1100);
                goto Start;
            }
        }

        private async void finishchar_Click(object sender, EventArgs e)
        {
            if(data > 0)
            {
                timer9.Start();
            }
            else
            {
                showcharmsg();
                charmsgtext(false, false, false, false, true, false);
                await Task.Delay(2000);
                timer8.Start();
            }
        }


        //PASS DATAS TO BOTWINDOW
        private Task<string> passdata()
        {
            exp.CharCount = data;
            exp.maplist = mapnames;
            exp.mapid = mapid;
            exp.detailedinfo = detailedinfo;

            for (int i = 0; i < data; i++)
            {
                string server = char.ToUpper(CharInfo[i].server[0]).ToString() + CharInfo[i].server.Substring(1);
                exp.settingchars.Items.Add(CharInfo[i].nick + " (" + CharInfo[i].level + "" + CharInfo[i].prof + ") [" + server + "]");
            }

            return Task.FromResult("ok");
        }

    }
}
