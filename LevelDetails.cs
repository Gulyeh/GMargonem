﻿using System;

namespace GMargonem
{
    class LevelDetails
    {
        public static long GetLowExp(int level)
        {
            if (level == 1)
                return 0L;
            return 10L + (long)Math.Pow(level - 1, 4.0);
        }

        public static long GetMaxExp(int level)
        {
            if (level == 1)
                return 1L;
            return 10L + (long)Math.Pow(level, 4.0);
        }

        public static int LevelProcent(int level, long myexp)
        {
            long maxexp = GetMaxExp(level);
            long lowexp = GetLowExp(level);

            long iletrzeba = maxexp - lowexp;
            long ilewbilem = (myexp - lowexp) * 100;
            double per = ilewbilem / iletrzeba;
            return (int)per;
        }
    }
}
