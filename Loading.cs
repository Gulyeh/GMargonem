﻿using System;
using System.Windows.Forms;

namespace GMargonem
{
    public partial class Loading : Form
    {
        public Loading()
        {
            InitializeComponent();
        }

        int dir = 1;
        
        private void loginani_Tick(object sender, EventArgs e)
        {
            if(loadinglogin.Value == 80)
            {
                dir -= 1;
                loadinglogin.animationIterval = 6;
            }
            else if(loadinglogin.Value == 20)
            {
                dir += 1;
                loadinglogin.animationIterval = 1;
            }

            loadinglogin.Value += dir;         
        }
        
        private void exitapp1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
