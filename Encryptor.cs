﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace GMargonem
{
    public class Encryptor
    {
        public static string ToSha(string content)
        {
            using (var sha1 = SHA1.Create())
            {
                var bytesToHash = Encoding.Default.GetBytes(content);
                return BitConverter.ToString(sha1.ComputeHash(bytesToHash)).Replace("-", string.Empty).ToLowerInvariant();
            }
        }

        public static string Md5(string blank, string salt)
        {
            byte[] buffer = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(salt + blank));
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < buffer.Length; i++)
            {
                builder.Append(buffer[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
