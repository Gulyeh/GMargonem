﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GMargonem
{
    public partial class AddCharMessage : Form
    {
        public AddCharMessage()
        {
            InitializeComponent();
            panel2.Left = 0;
        }

        int add = 2;
        public void move(object sender, EventArgs e)
        {
            panel2.Left += add+1;

            if (panel2.Left < 0)
            {
                add = 2;
            }
            else if (panel2.Left > 157)
            {
                add = -4;
            }
        }

        private void AddCharMessage_Load(object sender, EventArgs e)
        {
            timer1.Tick += new EventHandler(move);
            timer1.Start();
        }

    }
}
