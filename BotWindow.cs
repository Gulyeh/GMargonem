﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.IO;

namespace GMargonem
{
    public partial class BotWindow : Form
    {

        //GET VARIABLES
        public static string login { get; set; }
        public int CharCount { get; set; }
        public List<string>[] maplist { get; set; }
        public List<string>[] detailedinfo { get; set; }
        public string[] mapid { get; set; }

        //NEW VARIABLES
        public static int timetowait = 5;
        public int hpvalue = 80;
        public static List<SettVar> setting = new List<SettVar>();
        bool started = false;
        int expindex = 0;
        bool startedtimer = false;
        public static DateTime StartTime;


        public class SettVar{
            public bool autoheal;
            public bool checkpots;
            public string hpval;
            public bool sellitems;
            public bool sellpots;
            public bool equiparrow;
            public bool checkarrow;
            public bool usestamina;
            public bool usebon;
            public int mapindex;
        }


        //LOAD
        public BotWindow()
        {
            InitializeComponent();
            this.Height = 32;
            this.Enabled = false;
            settings.BackColor = Color.SteelBlue;
        }

        private async void BotWindow_Load(object sender, EventArgs e)
        {
            await adddetailedinfo();
            await DefaultSettings();
            timer1.Start();
        }

        private void BotWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        //BUTTONS,KEYS PRESSED,CHANGED ETC
        private void staminawait_TextChanged(object sender, EventArgs e)
        {
            if (staminawait.Text != String.Empty)
            {
                if (Int32.Parse(staminawait.Text) > 0 && Int32.Parse(staminawait.Text) <= 60)
                {
                    timetowait = Int32.Parse(staminawait.Text);
                }
                else
                {
                    staminawait.Text = timetowait.ToString();
                }
            }
        }

        private void hpproc_TextChanged(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (hpproc.Text != String.Empty)
            {
                if (Int32.Parse(hpproc.Text) > 0 && Int32.Parse(hpproc.Text) <= 99)
                {
                    hpvalue = Int32.Parse(hpproc.Text);
                }
                else
                {
                    hpproc.Text = hpvalue.ToString();
                }
            }

            setting[index].hpval = hpproc.Text;
        }

        private void staminawait_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void hpproc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void staminawait_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                staminawait.Enabled = false;
                staminawait.Enabled = true;
            }
        }

        private void hpproc_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                hpproc.Enabled = false;
                hpproc.Enabled = true;
            }
        }


        //BUTTONS
        private void minimalize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void exitapp_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void exp_Click(object sender, EventArgs e)
        {
            exp.BackColor = Color.SteelBlue;
            settings.BackColor = Color.FromArgb(64, 109, 151);
            details.BackColor = Color.FromArgb(64, 109, 151);
            tabControl1.SelectTab(0);
        }

        private void settings_Click(object sender, EventArgs e)
        {
            settings.BackColor = Color.SteelBlue;
            exp.BackColor = Color.FromArgb(64, 109, 151);
            details.BackColor = Color.FromArgb(64, 109, 151);
            tabControl1.SelectTab(1);
        }

        private void details_Click(object sender, EventArgs e)
        {
            details.BackColor = Color.SteelBlue;
            exp.BackColor = Color.FromArgb(64, 109, 151);
            settings.BackColor = Color.FromArgb(64, 109, 151);
            tabControl1.SelectTab(2);
        }

        //CHECKERS
        private void usebless_OnValueChange(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (usebless.Value == true)
            {
                setting[index].usebon = true;
            }
            else
            {
                setting[index].usebon = false;
            }
        }

        private void usestamina_OnValueChange(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (usestamina.Value == true)
            {
                setting[index].usestamina = true;
            }
            else
            {
                setting[index].usestamina = false;
            }
        }

        private void checkarrows_OnValueChange(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (checkarrows.Value == true)
            {
                setting[index].checkarrow = true;
            }
            else
            {
                setting[index].checkarrow = false;
            }
        }

        private void equiparrows_OnValueChange(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (equiparrows.Value == true)
            {
                setting[index].equiparrow = true;
                checkarrows.Enabled = true;
            }
            else
            {
                setting[index].equiparrow = false;
                checkarrows.Value = false;
                checkarrows.Enabled = false;
            }
        }

        private void autoheal_OnValueChange(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (autoheal.Value == true)
            {
                setting[index].autoheal = true;
                checkpots.Enabled = true;
                hpproc.Enabled = true;
            }
            else
            {
                setting[index].autoheal = false;
                checkpots.Value = false;
                hpproc.Enabled = false;
                checkpots.Enabled = false;
            }
        }

        private void sellitems_OnValueChange(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (sellitems.Value == true)
            {
                setting[index].sellitems = true;
                sellpots.Enabled = true;
            }
            else
            {
                setting[index].sellitems = false;
                sellpots.Value = false;
                sellpots.Enabled = false;               
            }
        }

        private void checkpots_OnValueChange(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (checkpots.Value == true)
            {
                setting[index].checkpots = true;
            }
            else
            {
                setting[index].checkpots = false;
            }
        }

        private void sellpots_OnValueChange(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            if (sellpots.Value == true)
            {
                setting[index].sellpots = true;
            }
            else
            {
                setting[index].sellpots = false;
            }
        }

        private void listofmaps_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;
            setting[index].mapindex = listofmaps.SelectedIndex;
        }

        //TIMERS
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Height < 342)
            {
                this.Height += 114;
            }
            else if (this.Height >= 342)
            {
                tabControl1.SelectTab(1);
                this.Enabled = true;
                timer1.Stop();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            TimeSpan elapsedTime = DateTime.Now - StartTime;
            time.Text = elapsedTime.ToString("d'd:'h'h:'m'm:'s's'");
        }

        //SETTINGS
        private async Task<bool> DefaultSettings()
        {
            for (int i = 0; i < CharCount; i++)
            {
                SettVar item = new SettVar
                {
                    autoheal = false,
                    checkpots = false,
                    hpval = "80",
                    sellitems = false,
                    sellpots = false,
                    equiparrow = false,
                    checkarrow = false,
                    usestamina = false,
                    usebon = false,
                    mapindex = 0
                };

                setting.Add(item);
            }

            if (File.Exists("Settings-"+login+".xml"))
            {
                savesett.Checked = true;
                await SaveSettings.ReadSettings();
                SetXMLSetting();
            }

            settingchars.SelectedIndex = 0;

            return true;
        }

        private void SetXMLSetting()
        {
            for (int i = 0; i < CharCount; i++)
            {
                var json = JObject.Parse(detailedinfo[i][0]);
                var id = json["h"]["id"].ToString();

                for (int j = 0; j < SaveSettings.LoadXML.Count; j++)
                {
                    if (id == SaveSettings.LoadXML[j].id)
                    {
                        setting[i].autoheal = SaveSettings.LoadXML[j].autoheal;
                        setting[i].hpval = SaveSettings.LoadXML[j].hpval;
                        setting[i].checkpots = SaveSettings.LoadXML[j].checkpots;
                        setting[i].sellitems = SaveSettings.LoadXML[j].sellitems;
                        setting[i].sellpots = SaveSettings.LoadXML[j].sellpots;
                        setting[i].equiparrow = SaveSettings.LoadXML[j].equiparrow;
                        setting[i].checkarrow = SaveSettings.LoadXML[j].checkarrow;
                        setting[i].usestamina = SaveSettings.LoadXML[j].usestamina;
                        setting[i].usebon = SaveSettings.LoadXML[j].usebon;
                        setting[i].mapindex = SaveSettings.LoadXML[j].mapindex;
                    }
                }
            }
        }

        private void settingchars_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = settingchars.SelectedIndex;

            //UPDATE MAPS
            listofmaps.Items.Clear();

            var iloscmap = mapid[index].Length;

            foreach(var mapname in maplist[index])
            {
                listofmaps.Items.Add(mapname);
            }

            listofmaps.SelectedIndex = setting[index].mapindex;


            //CHECK ARROW CLASS
            bool usingarrows = UsingArrowsClass(index);

            if(usingarrows == true)
            {
                equiparrows.Enabled = true;
                equiparrows.Visible = true;
            }
            else
            {
               setting[index].equiparrow = false;
               setting[index].checkarrow = false;
               equiparrows.Enabled = false;
               checkarrows.Enabled = false;
            }


            //APPLY TICKS AND VALUES IN SETTINGS
            UpdateOptions(index);


        }

        private void UpdateOptions(int index)
        {     
            autoheal.Value = setting[index].autoheal;
            checkpots.Value = setting[index].checkpots;
            hpproc.Text = setting[index].hpval;
            sellitems.Value = setting[index].sellitems;
            sellpots.Value = setting[index].sellpots;
            equiparrows.Value = setting[index].equiparrow;
            checkarrows.Value = setting[index].checkarrow;
            usestamina.Value = setting[index].usestamina;
            usebless.Value = setting[index].usebon;
            
        }

        private bool UsingArrowsClass(int index)
        {
            var json = JObject.Parse(detailedinfo[index][0]);
            if (json["h"]["prof"].ToString() == "h" || json["h"]["prof"].ToString() == "t")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //FILL DETAIL TAB
        private Task<bool> adddetailedinfo()
        {

            for (int i = 0; i < CharCount; i++)
            {
                var json = JObject.Parse(detailedinfo[i][0]);
                detinfo.Rows.Add();
                detinfo.Rows[i].Cells[0].Value = json["h"]["id"];
                detinfo.Rows[i].Cells[1].Value = json["h"]["nick"];
                detinfo.Rows[i].Cells[2].Value = json["h"]["lvl"];
                detinfo.Rows[i].Cells[3].Value = json["h"]["prof"];
                detinfo.Rows[i].Cells[4].Value = json["worldname"];
                detinfo.Rows[i].Cells[5].Value = json["h"]["uprawnienia"];
                detinfo.Rows[i].Cells[6].Value = json["h"]["ap"];
                detinfo.Rows[i].Cells[7].Value = json["h"]["bagi"];
                detinfo.Rows[i].Cells[8].Value = json["h"]["bint"];
                detinfo.Rows[i].Cells[9].Value = json["h"]["bstr"];
                detinfo.Rows[i].Cells[10].Value = json["h"]["clan"];
                detinfo.Rows[i].Cells[11].Value = json["h"]["credits"];
                detinfo.Rows[i].Cells[12].Value = json["h"]["runes"];
                detinfo.Rows[i].Cells[13].Value = json["h"]["dir"];
                detinfo.Rows[i].Cells[14].Value = json["h"]["exp"];
                detinfo.Rows[i].Cells[15].Value = json["h"]["gold"];
                detinfo.Rows[i].Cells[16].Value = json["h"]["goldlim"];
                detinfo.Rows[i].Cells[17].Value = json["h"]["honor"];
                detinfo.Rows[i].Cells[18].Value = json["h"]["pttl"];
                detinfo.Rows[i].Cells[19].Value = json["h"]["pvp"];
                detinfo.Rows[i].Cells[20].Value = json["h"]["ttl"];
                detinfo.Rows[i].Cells[21].Value = json["h"]["wanted"];
                detinfo.Rows[i].Cells[22].Value = json["h"]["stamina"];
                detinfo.Rows[i].Cells[23].Value = json["h"]["warrior_stats"]["hp"];
                detinfo.Rows[i].Cells[24].Value = json["h"]["warrior_stats"]["maxhp"];
                detinfo.Rows[i].Cells[25].Value = json["h"]["warrior_stats"]["st"];
                detinfo.Rows[i].Cells[26].Value = json["h"]["warrior_stats"]["ag"];
                detinfo.Rows[i].Cells[27].Value = json["h"]["warrior_stats"]["it"];
                detinfo.Rows[i].Cells[28].Value = json["h"]["warrior_stats"]["sa"];
                detinfo.Rows[i].Cells[29].Value = json["h"]["warrior_stats"]["crit"];
                detinfo.Rows[i].Cells[30].Value = json["h"]["warrior_stats"]["ac"];
                detinfo.Rows[i].Cells[31].Value = json["h"]["warrior_stats"]["resfire"];
                detinfo.Rows[i].Cells[32].Value = json["h"]["warrior_stats"]["resfrost"];
                detinfo.Rows[i].Cells[33].Value = json["h"]["warrior_stats"]["reslight"];
                detinfo.Rows[i].Cells[34].Value = json["h"]["warrior_stats"]["act"];
                detinfo.Rows[i].Cells[35].Value = json["h"]["warrior_stats"]["dmg"];
                detinfo.Rows[i].Cells[36].Value = json["h"]["warrior_stats"]["blok"];
                detinfo.Rows[i].Cells[37].Value = json["h"]["warrior_stats"]["critval"];
                detinfo.Rows[i].Cells[38].Value = json["h"]["warrior_stats"]["energy"];
            }

            return Task.FromResult(true);
        }

        private void startbutton_Click(object sender, EventArgs e)
        {
            if (savesett.Checked == true)
            {
                SaveSettings.id = detailedinfo;
                SaveSettings.SaveData(CharCount);
            }

            if (started == false)
            {
                startbutton.Text = "Stop";
                started = true;
                Initialization();
            }
            else if(started == true)
            {
                startbutton.Text = "Start";
                started = false;
                MainBot.Stop();
            }

        }

        //START BOTTING

        private Task<bool> SetItemsValues()
        {
            if(Game.iloscstrzal == 0)
            {
                arrowleft.Text = "-";
            }
            else
            {
                arrowleft.Text = Game.iloscstrzal.ToString();
            }
            potleft.Text = Game.iloscpotek.ToString();
            return Task.FromResult(true);
        }

        private void SetCharacterInfo(string name, string lvl, string hp, string maxhp, string exp, string stamina, string NextName)
        {
            charname.Text = name;
            charlevel.Text = lvl;
            charhp.Text = hp + "/" + maxhp;
            charstamina.Text = stamina + "/50";
            charexp.Text = exp + "/" + LevelDetails.GetMaxExp(Int32.Parse(lvl))+" ("+LevelDetails.LevelProcent(Int32.Parse(lvl), Convert.ToInt64(exp))+"%)";
            nextchar.Text = NextName;
        }

        private async void Initialization()
        {
            if (startedtimer == false)
            {
                startedtimer = true;
                StartTime = DateTime.Now;
                timer2.Start();
            }

            Start:
            try
            {

                var json = JObject.Parse(detailedinfo[expindex][0]);

                //VARIABLES
                string NextChar = "-";
                Game.prof = json["h"]["prof"].ToString();
                Game.lvl = Int32.Parse(json["h"]["lvl"].ToString());
                Game.ExpBless = setting[expindex].usebon;


                //GET NEXT CHARACTER NAME
                if(CharCount > 0)
                {
                    if (expindex == CharCount)
                    {
                        var json1 = JObject.Parse(detailedinfo[0][0]);
                        NextChar = json1["h"]["nick"].ToString();
                    }
                    else
                    {
                        var json1 = JObject.Parse(detailedinfo[expindex + 1][0]);
                        NextChar = json1["h"]["nick"].ToString();
                    }
                }

                //SET CHARACTER TEXT VALUES
                SetCharacterInfo(json["h"]["nick"].ToString(), json["h"]["lvl"].ToString(), json["h"]["warrior_stats"]["hp"].ToString(), json["h"]["warrior_stats"]["maxhp"].ToString(), json["h"]["exp"].ToString(), json["h"]["stamina"].ToString(), NextChar);


                //INITIALIZATION
                lastaction.Text = "[" + DateTime.Now + "] Getting Token";
                await Game.GetGameDetails(json["worldname"].ToString(), json["h"]["id"].ToString(), "1");
                await Task.Delay(1000);
                await Game.GetGameDetails(json["worldname"].ToString(), json["h"]["id"].ToString(), "2");
                await Task.Delay(1000);
                lastaction.Text = "[" + DateTime.Now + "] Getting Items";
                await Game.GetGameDetails(json["worldname"].ToString(), json["h"]["id"].ToString(), "3");
                await SetItemsValues();
                await Task.Delay(1000);
                lastaction.Text = "[" + DateTime.Now + "] Getting Event";
                await Game.GetGameDetails(json["worldname"].ToString(), json["h"]["id"].ToString(), "4");
                await Task.Delay(1000);
                //MainBot.Start();
            }
            catch(Exception)
            {
                goto Start;
            }

        }

        private void MainBot_Tick(object sender, EventArgs e)
        {
            try
            {

            }
            catch(Exception ex)
            {
                lastaction.Text = "[" + DateTime.Now + "] Error. Relogging.";
                var file = File.Create("GMargonem.Dump-" + DateTime.Now + ".txt");
                File.WriteAllText(file.ToString(), ex.ToString());
                Initialization();
                MainBot.Stop();
            }
        }

    }
}
